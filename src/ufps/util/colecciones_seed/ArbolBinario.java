/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBinario.java,v 2.0 2020/12/14 
 * Universidad Francisco de Paula Santander 
 * Programa Ingenieria de Sistemas
 * Estructuras de datos
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package ufps.util.colecciones_seed;
import java.util.Iterator;

/**
 * Implementacion de Clase para el manejo de un Arbol Binario.
 * @param <T> Tipo de datos a almacenar en el Arbol Binario.
 * @author Marco Adarme
 * @version 2.0
 */
public class ArbolBinario<T>
{
    
    ////////////////////////////////////////////////////////////
    // ArbolBinario - Atributos ////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Nodo raiz del Arbol Binario
     */
    private NodoBin<T> raiz;        
    
    

    ////////////////////////////////////////////////////////////
    // ArbolBinario - Implementacion de Metodos ////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Crea un Arbol Binario vacio. <br>
     * <b>post: </b> Se creo un Arbol Binario vacio.<br>
     */
     public ArbolBinario(){
        this.raiz=null;
     }
     
    /**
     * Crea un Arbol Binario con una raiz predefinida. <br>
     * <b>post: </b> Se creo un nuevo Arbol con su raiz definida.<br>
     * @param raiz  Un objeto de tipo T que representa del dato en la raiz del Arbol. <br>
     */
     public ArbolBinario(T raiz) {
        this.raiz = new NodoBin(raiz);
     }
     
    /**
     * Metodo que permite conocer el objeto de la raiz del Arbol Binario. <br>
     * <b>post: </b> Se obtuvo la raiz del Arbol Binario.<br>
     * @return la raiz del Arbol Binario.
     */
    public T getObjRaiz() {
        return (raiz.getInfo());
    }
     
    /**
     * Metodo que permite conocer la raiz del Arbol Binario. <br>
     * <b>post: </b> Se obtuvo la raiz del Arbol Binario.<br>
     * @return la raiz del Arbol Binario.
     */
    public NodoBin<T> getRaiz() {
        return raiz;
    }
    
    
    
    /**
     * Obtiene la información de las hojas que están a la derecha, por ejemplo:
     *   5       
        / \   
       /   \  
       1   6   
          / \ 
          2 4 
     *  retornaría: 4
     * @return un String con las hojas derechas
     */
    public String getInfoHojasDerechas()
    {
    return "";
    }
    
    
    
    
    /**
     * Metodo que permite modificar la raiz del Arbol Binario. <br>
     * <b>post: </b> Se modifico la raiz del Arbol Binario.<br>
     * @param raiz representa la nueva raiz del Arbol Binario.
     */
    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }
        
    
    
    /**
     * Obtiene la información de las hojas (Decorador)
     * @return un String con el info de las hojas
     */
    public String getInfoNodosRamas()
    {
    
        return this.getInfoNodosRamas(raiz);
    }
    
    
    
    private String getInfoNodosRamas(NodoBin<T> r)
    {
     //Caso base: 
       if(r==null) 
           return "";
              
       String info="";
       if(!this.esHoja(r))
           info=r.getInfo().toString()+",";
       
     //Llamado a la recursión: 
    return (info+this.getInfoNodosRamas(r.getIzq())+this.getInfoNodosRamas(r.getDer()));
    }
    
    
    
    
    /**
     * Obtiene la información de las hojas (Decorador)
     * @return un String con el info de las hojas
     */
    public String getHojas()
    {
    
        return this.getHojas(raiz);
    }
    
    
    
    private String getHojas(NodoBin<T> r)
    {
     //Caso base: 
       if(r==null) 
           return "";
              
       String info="";
       if(this.esHoja(r))
           info=r.getInfo().toString()+",";
       
     //Llamado a la recursión: 
    return (info+this.getHojas(r.getIzq())+this.getHojas(r.getDer()));
    }
    
    
    
    /**
     * Método que obtiene la cantidad de nodos hojas del árbol binario
     * @return un entero con la cantidad de hojas
     */
    public int getCantidadHojas()
    {
    
        NodoBin<T> r=this.raiz;
        int cant=this.getCantidadHojas(r);
        return cant;  // this.getCantidadHojas(this.raiz);
    }
    
    /**
     *  Método recursivo para contar hojas
     * @param r de tipo NodoBIn y representa la raíz de cada subárbol
     * @return un entero con la cantidad de hojas
     */
    private int getCantidadHojas(NodoBin<T> r)
    {
     //Caso base: 
       if(r==null) 
           return 0;
       int con=0;
       if(this.esHoja(r))
           con=1;
       
     //Llamado a la recursión: 
    return (con+this.getCantidadHojas(r.getIzq())+this.getCantidadHojas(r.getDer()));
    }
    
    private boolean esHoja(NodoBin<T> nodo)
    {
    
        return (nodo!=null && nodo.getIzq()==null && nodo.getDer()==null);
    }
    
    
   
    /**
     * Metodo que permite mostrar por consola la informacion del Arbol Binario. <br>
     * @param n Representa la raiz del ArbolBinario o de alguno de sus subarboles.
     */
    public void imprime(NodoBin<T> n) {
        T l = null;
        T r = null;
        if(n==null)
            return;
        if(n.getIzq()!=null) {
         l = n.getIzq().getInfo();
        }
        if(n.getDer()!=null) {
         r =n.getDer().getInfo();
        }       
        System.out.println("NodoIzq: "+l+"\t Info: "+n.getInfo()+"\t NodoDer: "+r);
        if(n.getIzq()!=null) {
         imprime(n.getIzq());
        }
        if(n.getDer()!=null) {
         imprime(n.getDer());
        }
    }
    
   
   
}//Fin de la Clase ArbolBinario
